# import time
# import unittest
# from selenium import webdriver


# class NewVisitorTest(unittest.TestCase):
#     def setUp(self):
#         self.browser = webdriver.Chrome()
#         self.browser.get("https://ppw-ki-nardienaap.herokuapp.com/")        
#         self.browser.maximize_window()
#         time.sleep(2)

#     def tearDown(self):
#         time.sleep(2)
#         self.browser.quit()


#     def test_can_insert_it_and_retrieve_it_later(self):
#         status_box = self.browser.find_element_by_name("status_content")
#         status_box.send_keys("This is a test for the status bar for Story 7.")
#         time.sleep(5)
#         submit_button = self.browser.find_element_by_id("submit_button")              # the submit button
#         submit_button.click()
#         time.sleep(5)
#         self.assertIn("This is a test for the status bar for Story 7.", self.browser.page_source)

#     def test_has_correct_title(self):
#         self.assertIn("Story 6", self.browser.title)

#     def test_hyperlink_nap_logo_works(self):
#         nap_logo = self.browser.find_element_by_id("nap_logo")
#         nap_logo.click()
#         time.sleep(5)
#         self.assertIn("Write your status! :]", self.browser.page_source)

#     def test_hyperlink_home_button_works(self):
#         home_button = self.browser.find_element_by_id("home_button")
#         home_button.click()
#         time.sleep(5)
#         self.assertIn("Write your status! :]", self.browser.page_source)

#     def test_hyperlink_profile_button_works(self):
#         profile_button = self.browser.find_element_by_id("profile_button")
#         profile_button.click()
#         time.sleep(5)
#         self.assertIn("Biodata", self.browser.page_source)


#     # for the css
#     def test_check_status_font_home(self):
#         element = self.browser.find_element_by_css_selector('h1')
#         self.assertEqual(element.value_of_css_property('font-size'), "40px")

#     def test_check_font_profile_page(self):
#         self.browser.get("http://localhost:8000/profilepage/")        
#         element = self.browser.find_element_by_css_selector('h3')
#         self.assertEqual(element.value_of_css_property('font-size'), "28px")

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')