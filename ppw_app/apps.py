from django.apps import AppConfig


class PpwAppConfig(AppConfig):
    name = 'ppw_app'
