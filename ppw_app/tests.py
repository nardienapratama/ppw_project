from django.test import TestCase, Client
from django.urls import resolve
from ppw_app.views import index, about, signup, schedule, deleteEvents
from .models import Schedule, Registration
from django.utils import timezone
import time


# Create your tests here.
class Ppw_appTest(TestCase):
    def test_ppw_app_url_is_exist(self):
        response = Client().get('/ppw_app/')
        self.assertEqual(response.status_code, 200)

    def test_ppw_app_using_index(self):
        response = Client().get('/ppw_app/')
        self.assertTemplateUsed(response, 'ppw_app/homepage.html')

    def test_ppw_app_using_about(self):
        response = Client().get('/ppw_app/about/')
        self.assertTemplateUsed(response, 'ppw_app/aboutme.html')

    def test_ppw_app_using_books(self):
        response = Client().get('/ppw_app/books/')
        self.assertTemplateUsed(response, 'ppw_app/books.html')

    def test_ppw_app_using_signup(self):
        response = Client().get('/ppw_app/signup/')
        self.assertTemplateUsed(response, 'ppw_app/signup.html')

    def test_ppw_app_using_schedule(self):
        response = Client().get('/ppw_app/schedule/')
        self.assertTemplateUsed(response, 'ppw_app/schedule.html')

    def test_model_can_create_new_schedule(self):
        # create a new schedule post
        new_schedule = Schedule.objects.create(eventName='OSUI Practice', category='OSUI', location='Pusgiwa')

        # Retrieve all schedule
        counting_all_available_schedule = Schedule.objects.all().count()
        self.assertEqual(counting_all_available_schedule, 1)

    def test_can_save_a_POST_request(self):
        response = Client().post('/ppw_app/schedule/', {'eventName' : 'OSUI Practice', 'category' : 'OSUI', 'location' : 'Pusgiwa'})
        counting_all_available_schedule = Schedule.objects.all().count()
        self.assertEqual(counting_all_available_schedule, 1)

    def test_model_can_create_reg_form(self):
        new_form = Registration.objects.create(firstname='Nardiena', lastname='Pratama', email='nardienapratama@yahoo.com', password='ppw')

        counting_all_available_forms = Registration.objects.all().count()
        self.assertEqual(counting_all_available_forms, 1)


        