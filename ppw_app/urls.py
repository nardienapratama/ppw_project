from django.urls import include, path
from django.contrib import admin
from .import views
# from .views import hello_function


urlpatterns = [
    # path ('admin/', admin.site.urls),
    #     path('ppw_app/', include('ppw_app.urls') ),
    path('', views.index, name='index'),
    path('about/',  views.about, name='about'),
    path('books/',  views.books, name='books'),    
    path('signup/', views.signup, name='signup'),
    path('schedule/', views.schedule, name='schedule'),
    # path('login/', views.login_my_view, name='loginview'),
    # path('logout/', views.logout, name='logoutview'),
    # path('userpage/', views.userpage, name='userpage'),
    path('deleteEvents/', views.deleteEvents, name='deleteEvents'),
    path('displayRegisForm/', views.displayRegisForm, name='displayRegisForm'),
    path('runForm/', views.runForm, name='runForm'),
    path('emailValidation/', views.emailValidation, name='emailValidation'),
    path('auth/', include('social_django.urls', namespace='social')),  # <- Here

    # path('hello/', views.hello_function, name='hello_function'),

    # url(r'^hello/', views.hello,name=='hello'),
]
    # url(r'^$', views.index,name=='index'),

