from django.shortcuts import render
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Schedule, Registration
from .forms import Activity_Form, Registration_Form
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
import json

# Create your views here.


def index(request):
        return render(request, 'ppw_app/homepage.html')
    

def about(request):
        return render(request, 'ppw_app/aboutme.html')

def books(request):
        # num_fav = request.POST['counter']
        user = request.user
        # user = authenticate(request, num_fav = num_fav)

        if user is not None:
                # num_visits = request.session.get('num_visits', 0)
                # request.session['num_visits'] = num_visits + 1

                user_session = request.session.get('user_session', 'private')
                request.session['user_session'] = 'private'

                # num_fav = request.POST['counter']
                # request.session['user_session']['favbooks'] = num_fav
                return render(request, 'ppw_app/books.html')

        else:
                del request.session['user_session']      
                return render(request, 'ppw_app/books.html')

def signup(request):
        return render(request, 'ppw_app/signup.html')

# def userpage(request):
#         response = render(request, 'ppw_app/books.html')
#         response.set_cookie('cool', 'cooler')
#         return render(response)

# def login_my_view(request):
        # username = request.POST['username']
        # password = request.POST['password']
        # user = authenticate(request, username=username, password=password)
        # if user is not None:
        #         login(request, user)

        #         return HttpResponseRedirect('/userpage/')
        # else:
        #         return HttpResponseRedirect('/userpage/')

# def logout_view(request):
# #     response.delete_cookie('cool')
#     logout(request)
#     # Redirect to a success page.
#     return HttpResponseRedirect('/userpage/')


def schedule(request):
        if request.method=="POST":
                form = Activity_Form(request.POST)
                if form.is_valid():
                        cleaned_data = form.cleaned_data
                        schedule = Schedule(eventName=cleaned_data['eventName'], date=cleaned_data['date'], time=cleaned_data['time'], category=cleaned_data['category'],
                                        location= cleaned_data['location'])
                        schedule.save()
                        return HttpResponseRedirect('/schedule')
                else:
                        return HttpResponseRedirect('/schedule')
        else:
                form = Activity_Form()
                return render(request, 'ppw_app/schedule.html', {'form':form,"schedules":Schedule.objects.all()})


def deleteEvents(request):
        Schedule.objects.all().delete()
        return HttpResponseRedirect('/schedule')


def displayRegisForm(request):
        response = {}
        form = Registration_Form(request.POST or None)
        response['form'] = form
        return render(request, 'ppw_app/registration.html', response)

def runForm(request):
        response = {}
        if request.method == 'POST':
                #         first_name = request.POST.get('firstname', None)
                #         last_name = request.POST.get('lastname', None)
                #         email = request.POST.get('email', None)
                #         password = request.POST.get('password', None)
                #         Registration.objects.create(firstname=first_name, lastname=last_name, email=email, password=password)
                #         return HttpResponse(json.dumps({'output': 'Thank you' + first_name + ' for subscribing!'}))
        
                form = Registration_Form(request.POST)
                if form.is_valid():
                        cleaned_data = form.cleaned_data
                        registrationinput = Registration(firstname=cleaned_data['firstname'], lastname=lastname, email=email, password=password)
                        registrationinput.save()
                        return JsonResponse(registrationinput)

                                
                else:
                        registrationinput = {}
                        return JsonResponse(registrationinput)
   
        
@csrf_exempt
def emailValidation(request):
        currentemail = request.POST.get('email', None)
        validation = {
                'isExist' : Registration.objects.filter(email= currentemail).exists()
        }
        return JsonResponse(validation)


