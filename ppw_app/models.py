from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Schedule(models.Model):
    eventName = models.CharField(max_length=50)
    date = models.DateField(default=timezone.now)
    time = models.TimeField(default=timezone.now)
    category = models.CharField(max_length=50)
    location = models.CharField(max_length=50)

class Registration(models.Model):
    firstname = models.CharField(max_length = 50)
    lastname = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50, unique=True)
    password = models.CharField(max_length = 50)

# class MyDiary(models.Model):
#     title = models.CharField(max_length=200)
#     body = models.TextField()
#     created_At = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return '%s %s' % (self.title, self.body)

# if(request.method == "POST"):
#     first_name = request.POST.get('first_name', None)

# function isValid(){ 
#     if(($("#id_email").val()==null || $("#id_email").val()=="") || 
#     ($("#id_password").val()==null || $("#id_password").val()=="") ||
#     ($("#id_firstName").val()==null || $("#id_firstName").val()=="") || 
#     ($("#id_lastName").val()==null || $("#id_lastName").val()=="")){ 
#         return false; 
#     } 
#     return true; 
# }