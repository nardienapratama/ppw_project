from django.urls import include, path
from django.conf.urls import url
from .views import index

#url for app
urlpatterns = [
    path('', index, name='index'),
]