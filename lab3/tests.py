# # Create your tests here.
# from django.test import TestCase, Client
# from django.urls import resolve
# from lab3.views import index
# from .models import Diary
# from django.utils import timezone

# class Lab3Test(TestCase):
#     # whether url exists
#     def test_lab_3_url_is_exist(self):
#         response = Client().get('/lab3/')  #call client() and when'/lab-3/' is found, it continues
#         self.assertEqual(response.status_code,200)  # '/lab-3/' 's status_code is called

#     #whether this template really works
#     def test_lab_3_using_to_do_list_template(self):
#         response = Client().get('/lab3/')
#         self.assertTemplateUsed(response, 'to_do_list.html')

#     def test_lab_3_using_index_func(self):
#         found = resolve('/lab3/')
#         self.assertEqual(found.func, index)

#     def test_model_can_Create_new_activity(self):
#         #Creating a new activity
#         new_activity = Diary.objects.create(date=timezone.now(), activity='I wanna practice coding')

#         #Retrieving all available activity
#         counting_all_available_activity = Diary.objects.all().count()
#         self.assertEqual(counting_all_available_activity, 1)

#     # stopped at step 17, haven't done it yet
#     def test_can_save_a_POST_request(self):
#         response = self.client.post('/lab3/add_activity', data={'date': '2018-10-10T10:00', 'activity' : 'Playing Dota'})
#         counting_all_available_activity = Diary.objects.all().count()
#         self.assertEqual(counting_all_available_activity, 1)

#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/lab33/')

#         new_response = self.client.get('/lab3/')
#         html_response = new_response.content.decode('utf8')
#         self.assertIn('Playing Dota', html_response)

