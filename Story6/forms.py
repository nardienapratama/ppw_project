from django import forms

class Status_Form(forms.Form):


    status_attrs = {
        'type' : 'text',
        'placeholder' : 'Status',
        'class' : 'form-control',
    }
    status_content = forms.CharField(widget= forms.TextInput(attrs=status_attrs))