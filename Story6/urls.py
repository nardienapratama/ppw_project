from django.urls import include, path
from django.conf.urls import url
from .views import index, profilepage, status, deleteEach

#url for app
urlpatterns = [
    path('', index, name='index'),
    path('profilepage/', profilepage, name='profilepage'),
    path('addstatus/', status, name='addstatus'),
    path('deleteEach/<int:id_name>', deleteEach, name='deleteEach' ),
]