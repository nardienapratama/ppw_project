from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from datetime import datetime
from .forms import Status_Form

# Create your views here.
def index(request):
    response = {'form':Status_Form, "statuses":Status.objects.all()}
    return render(request, 'index.html', response)
    # the response causes the Status_Form to appear when the key 'form' 
    # is included in the html

def profilepage(request):
    return render(request, 'profilepage.html')
    
# once you hit submit, it goes to this def
def status(request):
    if request.method=="POST":
        form = Status_Form(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data # cleaned_Data 'cleans' it, but not erase
            status = Status(status_content= cleaned_data['status_content'])
            status.save()
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')

def deleteEach(request, id_name):
    # gets the Status object that has id=id_name and deletes it only
    Status.objects.get(id=id_name).delete()
    return HttpResponseRedirect('/')

# make def just like in views.py in ppw app with POST and stuff

