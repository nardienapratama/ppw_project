from django.db import models

# Create your models here.
class Status(models.Model):
    status_content = models.TextField()
    id = models.AutoField(primary_key=True)
    # it causes each input to have an id

