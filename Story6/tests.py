from django.test import TestCase, Client
from django.urls import resolve
from Story6.views import index, profilepage, status, deleteEach
from .models import Status
from django.utils import timezone
import time
import unittest
from selenium import webdriver


# Create your tests here.
class Story6Test(TestCase):
    def test_Story6_url_is_exist(self):
        response = Client().get('/Story6/')
        self.assertEqual(response.status_code, 200)

    def test_Story6_using_index(self):
        response = Client().get('/Story6/')
        self.assertTemplateUsed(response, 'index.html')

    def test_Story6_using_index_func(self):
        found = resolve('/Story6/')
        self.assertEqual(found.func, index)



    
    def test_Story6_using_profilepage(self):
        response = Client().get('/Story6/profilepage/')
        self.assertTemplateUsed(response, 'profilepage.html')

    def test_Story6_using_profilepage_func(self):
        found = resolve('/Story6/profilepage/')
        self.assertEqual(found.func, profilepage)



    def test_model_can_create_new_status(self):    
        #Creating a new activity
        new_status = Status.objects.create(status_content='Helloo')

        #Retrieving all status
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_can_save_a_POST_request(self):
        response = Client().post('/Story6/addstatus/', {'status_content' : 'Helloo'})
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.get("https://ppw-ki-nardienaap.herokuapp.com/Story6/")        
        self.browser.maximize_window()
        time.sleep(2)

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()


    def test_can_insert_it_and_retrieve_it_later(self):
        status_box = self.browser.find_element_by_name("status_content")
        status_box.send_keys("This is a test for the status bar for Story 7.")
        time.sleep(5)
        submit_button = self.browser.find_element_by_id("submit_button")              # the submit button
        submit_button.click()
        time.sleep(5)
        self.assertIn("This is a test for the status bar for Story 7.", self.browser.page_source)

    def test_has_correct_title(self):
        self.assertIn("Story 6", self.browser.title)

    def test_hyperlink_nap_logo_works(self):
        nap_logo = self.browser.find_element_by_id("nap_logo")
        nap_logo.click()
        time.sleep(5)
        self.assertIn("Write your status! :]", self.browser.page_source)

    def test_hyperlink_home_button_works(self):
        home_button = self.browser.find_element_by_id("home_button")
        home_button.click()
        time.sleep(5)
        self.assertIn("Write your status! :]", self.browser.page_source)

    def test_hyperlink_profile_button_works(self):
        profile_button = self.browser.find_element_by_id("profile_button")
        profile_button.click()
        time.sleep(5)
        self.assertIn("Biodata", self.browser.page_source)


    # for the css
    def test_check_status_font_home(self):
        element = self.browser.find_element_by_css_selector('h1')
        self.assertEqual(element.value_of_css_property('font-size'), "40px")

    def test_check_font_profile_page(self):
        self.browser.get("http://localhost:8000/Story6/profilepage/")        
        element = self.browser.find_element_by_css_selector('h3')
        self.assertEqual(element.value_of_css_property('font-size'), "28px")

if __name__ == '__main__':
    unittest.main(warnings='ignore')
        # self.assertEqual(response.status_code, 302)
        # self.assertEqual(response['location'], '/')

        # new_response = Client().get('/')
        # html_response = new_response.content.decode('utf8')
        # self.assertIn('Helloo', html_response)
