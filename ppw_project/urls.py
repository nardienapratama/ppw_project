"""ppw_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path
# from django.views.generic.base import RedirectView
from ppw_app import urls as ppw_app
from Story6 import urls as Story6

# from Story6.views import index, profilepage, status, deleteEach
# from ppw_app.resources import MyDiaryResource
from lab3 import urls as lab3

# import ppw_app.urls as ppw_app_all

# mydiary_resource = MyDiaryResource()

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('ppw_app/', include('ppw_app.urls')),
    path('Story6/', include('Story6.urls')),
    path('lab3/', include('lab3.urls')),
    path('admin/', admin.site.urls),        
    # path('home/', include((ppw_app, 'home'), namespace='home')),
    # path('about/', include((ppw_app, 'about'), namespace='about')),
    # path('signup/', include((ppw_app, 'signup'), namespace='signup' )),

    # url(r'^$', include(ppw_app_all, namespace='somename'))
    
    # path('ppw_app', include(mydiary_resource.urls)),
    # path('lab3/', include((lab3, 'lab3'),namespace='lab3')),
    # path('', include('Story6.urls')),
    # path('', index, name='index'),
    # path('profilepage/', profilepage, name='profilepage'),
    # path('addstatus/', status, name='addstatus'),
    # path('deleteEach/<int:id_name>', deleteEach, name='deleteEach' ),
    
    
]

#     url(r'^admin/', admin.site.urls),
# 	url(r'^$', RedirectView.as_view(url= 'home/', permanent=True)),
# ]
